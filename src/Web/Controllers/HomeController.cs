﻿using Microsoft.AspNetCore.Mvc;

namespace Movies.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
