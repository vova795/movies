﻿using Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Data;
public class MovieContext : DbContext
{
    public MovieContext(DbContextOptions<MovieContext> options) :base(options)
    {
        
    }

    public DbSet<Movie> Movie { get; set; }

}
